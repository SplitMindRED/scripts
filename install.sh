#!/bin/bash

echo "Create src"
mkdir src
cd src

sudo apt install ros-noetic-tf -y
sudo apt install libglib2.0-dev -y

echo "Clone RaiSim"
git clone https://github.com/perchess/raisim_ros.git -b develop

echo "Clone controller"
git clone https://gitlab.com/rl-unitree-a1/be2r_cmpc_unitree.git

echo "Clone RaiSim driver"
git clone https://gitlab.com/rl-unitree-a1/raisim_unitree_ros_driver.git

echo "Clone legged sdk"
git clone https://gitlab.com/rl-unitree-a1/unitree_legged_sdk.git

echo "Clone legged msgs"
git clone https://gitlab.com/rl-unitree-a1/unitree_legged_msgs.git

echo "Clone gamepad control"
git clone https://gitlab.com/rl-unitree-a1/gamepad_control.git

echo "Clone unitree ros driver"
git clone https://gitlab.com/rl-unitree-a1/unitree_ros_driver.git

echo "Clone Eigen"
git clone https://gitlab.com/libeigen/eigen.git
cd eigen
mkdir build
cd build

echo "Build Eigen"
cmake ..
sudo make install
cd ../..

echo "Clone LCM"
git clone --depth 1 --branch v1.4.0 https://github.com/lcm-proj/lcm.git
cd lcm
mkdir build
cd build

echo "Build LCM"
cmake ..
sudo make install
cd ../../..

catkin_make --pkg unitree_legged_msgs
catkin_make

