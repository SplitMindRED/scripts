#!/bin/bash

echo "Uninstall eigen"
cd src/eigen/build
sudo make uninstall


echo "Uninstall lcm"
cd ../../lcm/build
sudo make uninstall
cd ../../..

sudo apt remove ros-noetic-tf -y
sudo apt remove libglib2.0-dev -y
